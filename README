The seetxt homepage: http://seetxt.sf.net.

If you are upgrading from a previous version of seetxt, please look
at NEWS, there is some important information there.

The rest of this file is intended for people new to the
"configure/make/install" process.  For more detailed information, 
see INSTALL.

To build on linux, cd into this directory (seetxt-0.72) and:

./configure

You'll see a bunch of checks happen.  If you do not have some prerequisite
software, you'll be notified (if configure goes okay, you can skip down
to MAKE). 

INSTALLING PREREQUISITES

Most likely you do not need to do this.

Seetxt uses common libraries, but these libraries have "devel" packages 
needed when building against them that are separate from the general 
runtime library; if you have never compiled a gtk+ 2.0 application on 
your system, you may need to install the gtk+ 2.0 development package.  
On debian based systems such as ubuntu:

apt-cache search libgtk2 | grep dev

Provides a list of candidates.  You want to install the package called 
"libgtk2.0-dev" appropriate to your system.  The equivalent on rpm based
systems will be called "gtk2-devel":

yum search gtk2 | grep devel

If you have never built anything from a source package on your system, 
you may also have to install "make".

MAKE

After running ./configure successfully, type:

make

This will produce some gobbledygook and possibly a few warnings depending on
your version of GCC, the compiler.  These have been accounted for in context
and can be ignored.

If make did not exit with an "Error", check in the "seetxt-0.72/src" directory 
to see that there is an executable called "seetxt" (not seetxt.o or seetxt.c).
If so, it's all good.  Now:

make install

to install seetxt system wide.  The documentation can now be found via:

man seetxt

You can delete the build directory containing this file after you make install,
but you may want to keep the source tarball, (seetxt-0.72.tar.bz2) so you can
easily uninstall later.  For further information, see the UNINSTALL file.
